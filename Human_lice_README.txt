Bubonic plague model with human arthropod vector

####################
##HUMAN Parameters##
####################

mu (μ) = 1/25*365 = .04/365 = human birth/death rate per day. 
From Keeling and Gilligan (2000)

gamma (γ) = 1/26.0 = is called the removal or recovery rate, though often we are more interested in its reciprocal (1/γ) which determines the average infectious period.
From Keeling and Gilligan (2000)

recovery_rate = .1
From Keeling and Gilligan (2000)

not_roaming = 80 percent of the day = 19.2 hours = time people are not coughing/being coughed on by neighbors.

########################
##LICE/FLEA Parameters##
########################

mu (μ) = 365/(30*365.0) = lice death rate per day. 
nu (𝛎) = 365/(40*365.0) = lice birth rate per day. 

gamma (γ) = 1/3.0 = is called the removal or recovery rate, though often we are more interested in its reciprocal (1/γ) which determines the average infectious period.Range (1/3.0 - 1/2.0) 
From Houhamdi et al (2006)

bite_rate = 1.0 = this is a bit confusing. This model is really set up for mosquitos so I am translating bite rate to number of feeding per day. From what I have read generally, lice can feed more than once per day but once seems to be a good minimum and seems to fit with the experimental model used by Houhamdi et al (2006) with the rabbits.

h.transmission_probability_v_to_h = = likelihood that a human will become infected
v.transmission_probability_h_to_v = 1.0 = likelihood that a louse will become infected
Houhamdi et al (2006)

lice_per_human = 20.0
Low estimate from several studies, see notes.

####################
##HUMAN Equations##
####################

dS = nu*h.S + nu*h.R - (not_roaming*bite_rate*h.transmission*l.I/l.N*h.S) - mu*h.S
dI = (not_roaming*bite_rate*v.transmission*.I/l.N*h.S) - (h.gamma*h.I) - mu*h.S
dR = recovery_rate*(h.gamma*h.I) - mu*h.R
dD = (h.gamma*h.I - recovery_rate*h.gamma*h.I)+ mu*h.S + mu*h.I + mu*h.R h.I

#######################
##LICE/FLEA Equations##
#######################

dS = nu*l.S - (not_roaming*bite_rate*transmission*h.I/h.N*l.S) - mu*l.S
dI = (not_roaming*bite_rate*transmission*h.I/h.N*l.S) - (l.gamma*l.I) - mu*l.S
dD = (l.gamma*l.I) + mu*l.S + mu*l.I
note: no recovery rate, these little buggers are gonna die

#################
##Between Cells##
#################

dI = roaming*bite_rate*transmission_rate_v_to_h*h.S*infectious_neighbors_lice/N_neighbors_lice