Model for PPP spread between humans without a vector.

Parameters:
mu (μ) = 1/25*365 = .04/365 = human birth/death rate per day. 
From Keeling and Gilligan (2000)

beta (β) = 562.1/365 = 1.54 = is the transmission rate and incorporates the encounter rate between susceptible and 
infectious individuals together with the probability of transmission.
From Nishiura et. al. (2006), calculated from R0 = 3.5

now varying beta between .4 and 1.4 to cover the estimated ranges of R0 in Nishura et. al. 2012

sigma (σ) = 1/4.3 = .233 = is the rate at which individuals move from the exposed to the infectious classes. Its 
reciprocal (1/σ) is the average latent (exposed) period.
From Gani and Leach (2004)

gamma (γ) = 1/2.5 = 0.4 = is called the removal or recovery rate, though often we are more interested in its 
reciprocal (1/γ) which determines the average infectious period.
From Gani and Leach (2004)

not_roaming = 90 percent of the day = 21.6 hours = time people are not coughing/being coughed on by neighbors.
This is completeley guestimated.

Equations:
MAIN SET SEIR (frequency dependent):
dS = mu*S - not_roaming*beta*S*I/N - mu*S
dE = not_roaming*beta*S*I/N - sigma*E - mu*E
dI = sigma*E - gamma*I - mu*I
dR = derp.
dD = gamma*I + mu*S + mu*E + mu*I

Between cells:
dE = Time_spent_roaming*beta*S*I(neighboring)/N(neighboring)
